import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Axios from "axios";
import "./config/fontawesome";
import "vue-slider-component/theme/default.css";

Vue.config.productionTip = false;

Vue.prototype.$http = Axios;

console.log(window.location.hostname.concat(":3000"));
store.commit("SET_API", "https://" + window.location.hostname.concat(":3000/"));
store.commit(
  "SET_SOCKET_API",
  "https://" + window.location.hostname.concat(":3001/")
);
store.dispatch("user/createUser");

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App)
}).$mount("#app");
