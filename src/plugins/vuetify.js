import Vue from "vue";
import Vuetify from "vuetify/lib";
import fr from "vuetify/es5/locale/fr";

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: { fr },
    current: "fr"
  },
  theme: {
    dark: false,
    light: true,
    themes: {
      light: {
        primary: "#0E5381",
        secondary: "#FAFAEF",
        accent: "#3B7EAB",
        error: "#EB5757"
      },
      dark: {
        primary: "#333333"
      }
    }
  }
});
