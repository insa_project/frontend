// import axios from "axios";

const meet = {
  namespaced: true,
  state: () => ({
    participants: [],
    joined: false,
    room: "général",
    socket: null,
    messages: []
  }),
  getters: {
    api(state, getters, rootState, rootGetters) {
      return rootGetters.socketApi;
    },
    participants(state) {
      return state.participants;
    },
    joined(state) {
      return state.joined;
    },
    room(state) {
      return state.room;
    },
    socket(state) {
      return state.socket;
    },
    messages(state) {
      return state.messages;
    }
  },
  actions: {
    setParticipants({ commit }, list) {
      commit("SET_PARTICIPANTS", list);
    },
    setJoined({ commit }, isJoined) {
      commit("SET_JOINED", isJoined);
    },
    setRoom({ commit }, name) {
      commit("SET_ROOM", name);
    },
    setSocket({ commit }, id) {
      commit("SET_SOCKET", id);
    },
    addMessage({ commit }, message) {
      commit("ADD_MESSAGE", message);
    }

    ///////// Post section ////////
  },
  mutations: {
    SET_PARTICIPANTS(state, list) {
      state.participants = list;
    },
    SET_JOINED(state, isJoined) {
      state.joined = isJoined;
    },
    SET_ROOM(state, name) {
      state.room = name;
    },
    SET_SOCKET(state, id) {
      state.socket = id;
    },
    ADD_MESSAGE(state, message) {
      state.messages.push(message);
    }
  }
};
export default meet;
