import axios from "axios";
import test from "./test";

const config = {
  namespaced: true,
  state: () => ({
    requestError: null,
    requestPending: null,
    requestSuccess: null,
    dark: false,
    publicIp: localStorage.getItem("publicIp") || ""
  }),
  getters: {
    api(state, getters, rootState, rootGetters) {
      return rootGetters.api + "config/";
    },
    publicIp(state) {
      return state.publicIp;
    },
    requestPending(state) {
      return state.requestPending;
    },
    requestError(state) {
      return state.requestError;
    },
    requestSuccess(state) {
      return state.requestSuccess;
    }
  },
  actions: {
    resetRequestError({ commit }) {
      commit("RESET_REQUEST_ERROR");
    },
    resetRequestPending({ commit }) {
      commit("SET_REQUEST_PENDING");
    },
    resetRequestSuccess({ commit }) {
      commit("RESET_REQUEST_SUCCESS");
    },
    ///////// Get section ////////

    ///////// Post section ////////
    postExpose({ commit, dispatch, getters }, expose) {
      let data = { state: expose };
      return new Promise((resolve, reject) => {
        axios({
          url: getters.api + "expose",
          data,
          method: "POST"
        })
          .then((resp) => {
            if (resp.data.publicIp != "") {
              commit(
                "test/SET_REQUEST_SUCCESS",
                "serveur ouvert à l'adresse" + resp.data.publicIp
              );
              dispatch(
                "setPublicIp",
                "https://" + resp.data.publicIp + ":3000/"
              );
            } else {
              commit("test/SET_REQUEST_SUCCESS", "serveur fermé");
              dispatch("setPublicIp", "");
            }
            resolve(resp);
          })
          .catch((err) => {
            commit("test/SET_REQUEST_ERROR", err);
            reject(err);
          });
      });
    },
    setPublicIp({ commit }, publicIp) {
      commit("SET_PUBLIC_IP", publicIp);
    }
  },
  mutations: {
    SET_PUBLIC_IP(state, ip) {
      state.publicIp = ip;
      if (ip == "") {
        state.publicIp = "";
        localStorage.removeItem("publicIp");
      } else localStorage.setItem("publicIp", ip);
    },
    SET_REQUEST_PENDING(state) {
      state.requestPending = !state.requestPending;
    }
  },
  modules: {
    test
  }
};
export default config;
