import axios from "axios";
import Vue from "vue";

const user = {
  namespaced: true,
  state: () => ({
    username: null,
    role: null,
    token: null
  }),
  getters: {
    api(state, getters, rootState, rootGetters) {
      return rootGetters.api + "user/";
    },
    username(state) {
      return state.username;
    },
    role(state) {
      return state.role;
    },
    token(state) {
      return state.token;
    }
  },
  actions: {
    ///////// Get section ////////
    setUserRole({ commit }) {
      if (window.location.hostname.startsWith("192.168")) {
        commit("ADD_ROLE", "host");
      } else {
        commit("ADD_ROLE", "participant");
      }
    },

    ///////// Get section ////////

    ///////// Post section ////////
    async cleanUser({ commit, dispatch }) {
      delete Vue.prototype.$http.defaults.headers["Authorization"];
      commit("CLEAN_USER");
      dispatch("setUserRole");
    },
    async createUser({ commit, dispatch, rootGetters }) {
      // Check if first use
      if (localStorage.getItem("user") != null) {
        const saveUser = JSON.parse(localStorage.getItem("user"));

        // Check if user is logon
        if (saveUser.username != null && saveUser.token != null) {
          Vue.prototype.$http.defaults.headers["Authorization"] =
            "Bearer " + saveUser.token;
          await axios
            .get(rootGetters.api + "whoAmI/")
            .then((response) => {
              // Token valid
              if (response.status == 200) {
                console.log("satus 200");
                commit("SET_USER", saveUser);
                dispatch("setConnected", true, { root: true });
              }
            })
            .catch((error) => {
              console.log("not valid user", error);
              delete Vue.prototype.$http.defaults.headers["Authorization"];
              commit("SET_USER", null);
            });
        } else {
          commit("SET_USER", null);
        }
      }
      dispatch("setUserRole");
    }
  },
  mutations: {
    SET_USERNAME(state, name) {
      state.username = name;
    },
    UPDATE_USER_DATA(state, data) {
      state.username = data.username;
      state.token = data.token;
      localStorage.setItem("user", JSON.stringify(state));
    },
    SET_USER(state, data) {
      if (data == null) {
        localStorage.setItem("user", null);
      } else {
        state.username = data.username;
        state.token = data.token;
        state.role = data.role;
      }
    },
    ADD_ROLE(state, role) {
      state.role = role;
      localStorage.setItem("user", JSON.stringify(state));
    },
    CLEAN_USER(state) {
      localStorage.removeItem("user");
      state.username = null;
      state.token = null;
      state.role = null;
    }
  }
};
export default user;
