import axios from "axios";

const test = {
  namespaced: true,
  state: () => ({
    requestError: null,
    requestSuccess: null
  }),
  getters: {
    api(state, getters, rootState, rootGetters) {
      return rootGetters.api + "ping/";
    },
    requestError(state) {
      return state.requestError;
    },
    requestSuccess(state) {
      return state.requestSuccess;
    }
  },
  actions: {
    resetRequestError({ commit }) {
      commit("RESET_REQUEST_ERROR");
    },
    resetRequestSuccess({ commit }) {
      commit("RESET_REQUEST_SUCCESS");
    },
    ///////// Get section ////////

    pingApi({ commit, getters }) {
      axios
        .get(getters.api)
        .then((response) =>
          commit("SET_REQUEST_SUCCESS", response.data.greeting)
        )
        .catch((error) => commit("SET_REQUEST_ERROR", error));
    }

    ///////// Post section ////////
  },
  mutations: {
    SET_REQUEST_ERROR(state, error) {
      state.requestSuccess = null;
      state.requestError = error;
    },
    RESET_REQUEST_ERROR(state) {
      state.requestError = null;
    },
    SET_REQUEST_SUCCESS(state, success) {
      state.requestError = null;
      state.requestSuccess = success;
    },
    RESET_REQUEST_SUCCESS(state) {
      state.requestSuccess = null;
    }
  }
};
export default test;
