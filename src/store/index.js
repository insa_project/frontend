import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import user from "./modules/user";
import config from "./modules/config";
import test from "./modules/test";
import meet from "./modules/meet";
import whiteboardModule from "./modules/whiteboard";

Vue.use(Vuex);

export default new Vuex.Store({
  state: () => ({
    api: "http://127.0.0.1:3000/",
    socketApi: "http://127.0.0.1:3001/",
    items: [
      { id: 0, tab: "Session en cours" }
      // { id: 1, tab: "Classe" },
      // { id: 2, tab: "Devoirs" }
    ],
    loginRequest: false,
    registerRequest: false,
    itemSelect: 0,
    connected: false,
    status: ""
  }),
  getters: {
    // Quand l'état de l'api, connected, items va changer mis à jour automatiquement
    api(state) {
      return state.api;
    },
    socketApi(state) {
      return state.socketApi;
    },
    connected(state) {
      return state.connected;
    },
    items(state) {
      return state.items;
    },
    itemSelect(state) {
      return state.itemSelect;
    },
    loginRequest(state) {
      return state.loginRequest;
    },
    registerRequest(state) {
      return state.registerRequest;
    }
  },
  actions: {
    setConnected({ commit }, connect) {
      commit("SET_CONNECTED", connect);
    },
    setItemSelect({ commit }, item) {
      commit("SET_ITEM_SELECT", item);
    },

    // SET UP VUEX ACTIONS

    setLoginRequest({ commit }) {
      commit("SET_LOGIN_REQUEST");
    },
    login({ commit, getters }, user) {
      let data = {
        email: user.email,
        password: user.password
      };
      return new Promise((resolve, reject) => {
        axios({
          url: getters.api + "users/login",
          data,
          method: "POST"
        })
          .then((resp) => {
            axios.defaults.headers.common["Authorization"] = resp.data.token;
            resolve(resp);
            commit("SET_CONNECTED", true);
            commit("test/SET_REQUEST_SUCCESS", "Connecté");
            let username = String(user.email).split("@")[0];
            commit("user/UPDATE_USER_DATA", {
              token: resp.data.token,
              username
            });
          })
          .catch((err) => {
            console.log("resp", err.response.data.error.message);
            commit("test/SET_REQUEST_ERROR", err.response.data.error.message);
            commit("SET_CONNECTED", false);
            reject(err);
          });
      });
    },
    setRegisterRequest({ commit }) {
      commit("SET_REGISTER_REQUEST");
    },
    register({ commit, getters }, user) {
      let data = {
        email: user.email,
        password: user.password
      };
      return new Promise((resolve, reject) => {
        axios({
          url: getters.api + "signup",
          data,
          method: "POST"
        })
          .then((resp) => {
            // Add the following line:
            commit("test/SET_REQUEST_SUCCESS", "Compte enregistré");
            resolve(resp);
            commit("SET_CONNECTED", false);
          })
          .catch((err) => {
            commit("test/SET_REQUEST_ERROR", err);
            localStorage.removeItem("token");
            reject(err);
            commit("SET_CONNECTED", false);
          });
      });
    },
    logout({ commit, dispatch }) {
      dispatch("user/cleanUser");
      commit("SET_CONNECTED", false);
    }
  },

  mutations: {
    SET_CONNECTED(state, connect) {
      state.connected = connect;
    },
    SET_API(state, url) {
      state.api = url;
    },
    SET_SOCKET_API(state, url) {
      state.socketApi = url;
    },
    SET_ITEM_SELECT(state, item) {
      state.itemSelect = item;
    },
    SET_LOGIN_REQUEST(state) {
      state.loginRequest = !state.loginRequest;
    },
    SET_REGISTER_REQUEST(state) {
      state.registerRequest = !state.registerRequest;
    },
    AUTH_ERROR(state) {
      state.status = "error";
    },
    AUTH_SUCCESS(state) {
      state.status = "success";
    }
  },
  modules: {
    user,
    config,
    test,
    meet,
    whiteboardModule
  }
});
