var express = require("express");
var app = express();
var server = app.listen(3001);
var io = require("socket.io").listen(server);

/**
 * Gestion des requêtes HTTP des utilisateurs en leur renvoyant les fichiers du dossier 'public'
 */
app.use(express.static("../dist"));

io.on("connection", function(socket) {
  /**
   * Log de connexion et de déconnexion des utilisateurs
   */
  console.log("a user connected");
  socket.on("disconnect", function() {
    console.log("user disconected");
  });

  /**
   * Réception de l'événement 'chat-message' et réémission vers tous les utilisateurs
   */
  socket.on("SEND_MESSAGE", (data) => {
    io.emit("MESSAGE", data);
  });
});
