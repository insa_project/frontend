# openvisio Front

## Install dependencies

By default, dependencies were installed when this application was generated.
Whenever dependencies in `package.json` are changed, run the following command:

```sh
npm install
```

## Run the application

```sh
npm run serve
```

Open https://127.0.0.1:8080 in your browser.

## Rebuild the project

To incrementally build the project:

```
npm run build
```

## Fix code style and formatting issues

If `eslint` and `prettier` are enabled for this project, you can use the
following commands to check code style and formatting issues.

```sh
npm run lint
```

## Other useful commands

- `npm run start-back`: Create and start the backend
- `npm run stop-back`: Stop the backend 


## Tests

```sh
npm run test:unit
```
